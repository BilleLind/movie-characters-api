package com.example.moviecharactersapi.mappers;

import com.example.moviecharactersapi.models.Franchise;
import com.example.moviecharactersapi.models.Movie;
import com.example.moviecharactersapi.models.dtos.FranchiseDTO;
import com.example.moviecharactersapi.services.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDTO(Franchise franchise);

    public abstract Collection<FranchiseDTO> franchiseToFranchiseDTOS(Collection<Franchise> franchises);

    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise (FranchiseDTO dto);

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> movies) {
        if(movies == null) {
            return null;
        }
        return movies.stream().map(movie -> movie.getId()).collect(Collectors.toSet());
    }

    @Named("movieIdsToMovies")
    Set<Movie> mapMovieIdsToMovies(Set<Integer> movieIds) {
        if (movieIds == null) {
            return null;
        }
        return movieIds.stream().map(id -> movieService.findById(id))
                .collect(Collectors.toSet());
    }


}
