package com.example.moviecharactersapi.runner;

import com.example.moviecharactersapi.models.Movie;
import com.example.moviecharactersapi.services.MovieService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class AppRunner implements ApplicationRunner {
    private final MovieService movieService;

    public AppRunner(MovieService movieService) {
        this.movieService = movieService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println("AaaaaaaaaaaAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASSSSSSSSSSSSSSS");
        ArrayList<Movie> movies = (ArrayList<Movie>) movieService.findAll();
        for (Movie m : movies) {
            System.out.println(m.toString());
        }
    }
}
