package com.example.moviecharactersapi.Controller;

import com.example.moviecharactersapi.mappers.FranchiseMapper;
import com.example.moviecharactersapi.models.Franchise;
import com.example.moviecharactersapi.models.dtos.CharacterDTO;
import com.example.moviecharactersapi.models.dtos.FranchiseDTO;
import com.example.moviecharactersapi.models.dtos.MovieDTO;
import com.example.moviecharactersapi.services.FranchiseService;
import com.example.moviecharactersapi.services.MovieService;
import com.example.moviecharactersapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieService movieService;


    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieService movieService) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieService = movieService;
    }

    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class))) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })

    @GetMapping
    public ResponseEntity getAll(){
        Collection<FranchiseDTO> dtos = franchiseMapper.franchiseToFranchiseDTOS(
                franchiseService.findAll()
        );
        return ResponseEntity.ok(dtos);
    }

    @Operation(summary = "Get a franchise by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FranchiseDTO.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with supplied ID",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class)) })
    })
    @GetMapping("{id}")
    public ResponseEntity getByid(@PathVariable int id) {
        FranchiseDTO dto = franchiseMapper.franchiseToFranchiseDTO(
                franchiseService.findById(id)
        );
        return ResponseEntity.ok(dto);
    }
    @Operation(summary = "Get a franchise by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = FranchiseDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "franchise does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })
    @GetMapping("search")
    public ResponseEntity<Collection<FranchiseDTO>> findByName(@RequestParam String name){
        Collection<FranchiseDTO> dtos = franchiseMapper.franchiseToFranchiseDTOS(
                franchiseService.findAllByName(name)
        );
        return ResponseEntity.ok(dtos);
    }

    @Operation(summary = "Adds a new franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "franchise successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) })
    })
    @PostMapping
    public ResponseEntity add(@RequestBody Franchise franchise) {
        Franchise f = franchiseService.add(franchise);
        URI location = URI.create("franchises/"+ f.getId());
        return ResponseEntity.created(location).build();
    }


    /*@PutMapping("{id}")
    public ResponseEntity update(@RequestBody FranchiseDTO franchiseDTO, @PathVariable int id) {
        if (id != franchiseDTO.getId()) {
            return ResponseEntity.badRequest().build();
        }
        franchiseService.update(
                franchiseMapper.franchiseDtoToFranchise(franchiseDTO)
        );
        return ResponseEntity.noContent().build();
    }*/

    @Operation(summary = "Deletes a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "franchise successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Updates a franchise")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "franchise successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class)) }),
            @ApiResponse(responseCode = "404",
                    description = "franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}/movies")
    public ResponseEntity updateMovies(@RequestBody Collection<Integer> movieIds, @PathVariable int id) {
        // find the franchise by the id
        // update the movies of the franchise
        // => should not be located in the controller, but repository or service

        Franchise franchise = franchiseService.findById(id);
        for (int mid: movieIds) {
            franchise.addMovie(movieService.findById(mid));
        }

        return ResponseEntity.ok(franchiseService.update(franchise));
    }

}
