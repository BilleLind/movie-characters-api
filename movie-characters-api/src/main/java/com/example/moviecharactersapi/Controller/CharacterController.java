package com.example.moviecharactersapi.Controller;

import com.example.moviecharactersapi.mappers.CharacterMapper;
import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.models.dtos.CharacterDTO;
import com.example.moviecharactersapi.models.dtos.MovieDTO;
import com.example.moviecharactersapi.services.CharacterService;
import com.example.moviecharactersapi.util.ApiErrorResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.net.URI;
import java.util.Collection;

@RestController
@RequestMapping(path = "api/v1/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;


    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }
    @Operation(summary = "Get all characters")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @GetMapping // GET: localhost:8080/api/v1/characters
    public ResponseEntity getAll(){
        Collection<CharacterDTO> dtos = characterMapper.characterToCharacterDTOS(
                characterService.findAll()
        );
        return ResponseEntity.ok(dtos);
    }

    @Operation(summary = "Get a Character by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = CharacterDTO.class))}),
            @ApiResponse(responseCode = "404",
                    description = "Character does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                    schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @GetMapping("{id}") // GET: localhost:8080/api/v1/characters/1
    public ResponseEntity getByid(@PathVariable int id) {
        CharacterDTO dto = characterMapper.characterToCharacterDTO(
                characterService.findById(id)
        );
        return ResponseEntity.ok(dto);
    }

    @Operation(summary = "Get a character by name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = CharacterDTO.class)))}),
            @ApiResponse(responseCode = "404",
                    description = "Movie does not exist with supplied ID",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiErrorResponse.class))})
    })

    @GetMapping("search") // GET: localhost:8080/api/v1/characters/1
    public ResponseEntity<Collection<CharacterDTO>> findByName(@RequestParam String name){
        Collection<CharacterDTO> dtos = characterMapper.characterToCharacterDTOS(
                characterService.findAllByName(name)
        );
        return ResponseEntity.ok(dtos);
    }

    @Operation(summary = "Adds a new character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "201",
                    description = "character successfully added",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))})
    })

    @PostMapping // POST: localhost:8080/api/v1/characters
    public ResponseEntity add(@RequestBody Character character) {
        Character c = characterService.add(character);
        URI location = URI.create("character/"+ c.getId());
        return ResponseEntity.created(location).build();
    }

    /*@PutMapping("{id}") // PUT: localhost:8080/api/v1/characters/1
    public ResponseEntity update(@RequestBody CharacterDTO characterDTO, @PathVariable int id) {
        if (id != characterDTO.getId()) {
            return ResponseEntity.badRequest().build();
        }
        characterService.update(
                characterMapper.characterDtoToCharacter(characterDTO)
        );
        return ResponseEntity.noContent().build();
    }*/

    @Operation(summary = "Deletes a character")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "character successfully deleted",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = ErrorAttributeOptions.class))}),
            @ApiResponse(responseCode = "404",
                    description = "character not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/characters/1
    public ResponseEntity delete(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }



            @Operation(summary = "Updates a character")
            @ApiResponses( value = {
                    @ApiResponse(responseCode = "204",
                            description = "character successfully updated",
                            content = @Content),
                    @ApiResponse(responseCode = "400",
                            description = "Malformed request",
                            content = {@Content(mediaType = "application/json",
                                    schema = @Schema(implementation = ErrorAttributeOptions.class))}),
                    @ApiResponse(responseCode = "404",
                            description = "character not found with supplied ID",
                            content = @Content)
            })

    @PutMapping("{id}/movies") // PUT: http://localhost:8080/api/v1/characters/*/movies
    public ResponseEntity updateMovies(@RequestBody Collection<Integer> movieIds, @PathVariable int id) {
        // find the character by the id
        // update the movies of the character
        // => should not be located in the controller, but repository or service

        return null;
    }

}