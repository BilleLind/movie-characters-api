package com.example.moviecharactersapi.models.dtos;

import lombok.Data;

import java.util.Set;

@Data
public class MovieDTO {
    private int id;
    private String title;
    private String genre;
    private String year;
    private String director;
    private String picture;
    private String trailer;
    private int franchise;
    private Set<Integer> characters;
}
