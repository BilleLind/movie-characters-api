package com.example.moviecharactersapi.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Character {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 50)
    String name;
    @Column(length = 50)
    String alias;
    @Column(length = 50)
    String gender;
    @Column(length = 250)
    String picture;
    @ManyToMany(mappedBy = "characters")
    Set<Movie> movies;
}
