package com.example.moviecharactersapi.models;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 50)
    String name;
    @Column(length = 250)
    String description;
    @OneToMany(mappedBy = "franchise")
    Set<Movie> movies;
    @Override
    public String toString() {
        return "Franchise{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }


    public void addMovie(Movie movie) {
        this.movies.add(movie);
        movie.setFranchise(this);
    }
}
