package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Movie;
import org.springframework.stereotype.Service;

@Service
public interface MovieService extends CrudService<Movie,Integer> {
}
