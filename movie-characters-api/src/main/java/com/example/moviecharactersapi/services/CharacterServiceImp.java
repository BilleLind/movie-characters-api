package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.repositories.CharacterRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class CharacterServiceImp implements CharacterService {
    private final CharacterRepository characterRepository;

    public CharacterServiceImp(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public Character findById(Integer integer) {
        return characterRepository.findById(integer).get();
    }

    @Override
    public Collection<Character> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public Character add(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public Character update(Character entity) {
        return characterRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        characterRepository.deleteById(integer);
    }

    @Override
    public void delete(Character entity) {
        characterRepository.delete(entity);
    }

    @Override
    public Collection<Character> findAllByName(String name) {
        return null;
    }
}
