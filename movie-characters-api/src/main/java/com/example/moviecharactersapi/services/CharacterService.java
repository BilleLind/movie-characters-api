package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.models.Franchise;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public interface CharacterService extends CrudService<Character,Integer> {

    Collection<Character> findAllByName(String name);
}