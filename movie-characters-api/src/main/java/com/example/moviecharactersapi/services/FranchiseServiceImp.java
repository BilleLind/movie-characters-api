package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Franchise;
import com.example.moviecharactersapi.models.dtos.FranchiseDTO;
import com.example.moviecharactersapi.repositories.FranchiseRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class FranchiseServiceImp implements FranchiseService {
    private final FranchiseRepository franchiseRepository;

    public FranchiseServiceImp(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    @Override
    public Franchise findById(Integer integer) {
        return franchiseRepository.findById(integer).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }



    @Override
    public Franchise add(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public Franchise update(Franchise entity) {
        return franchiseRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        franchiseRepository.deleteById(integer);
    }

    @Override
    public void delete(Franchise entity) {
        franchiseRepository.delete(entity);
    }


    @Override
    public Collection<Franchise> findAllByName(String name) {
        return franchiseRepository.findAllByName(name);
    }
}
