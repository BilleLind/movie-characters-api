package com.example.moviecharactersapi.services;

import com.example.moviecharactersapi.models.Movie;
import com.example.moviecharactersapi.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import java.util.Collection;

@Service
public class MovieServiceImp implements MovieService {
    private final MovieRepository movieRepository;

    public MovieServiceImp(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    @Override
    public Movie findById(Integer integer) {
        return movieRepository.findById(integer).get();
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public Movie update(Movie entity) {
        return movieRepository.save(entity);
    }

    @Override
    public void deleteById(Integer integer) {
        movieRepository.deleteById(integer);
    }

    @Override
    public void delete(Movie entity) {
        movieRepository.delete(entity);
    }
}