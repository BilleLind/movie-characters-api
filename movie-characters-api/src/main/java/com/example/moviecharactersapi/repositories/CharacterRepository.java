package com.example.moviecharactersapi.repositories;

import com.example.moviecharactersapi.models.Character;
import com.example.moviecharactersapi.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;


public interface CharacterRepository extends JpaRepository<Character,Integer> {

    @Query("select c from Character c where c.name like %?1%")
    Set<Franchise> findAllByName(String name);
}
