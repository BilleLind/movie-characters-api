package com.example.moviecharactersapi.repositories;

import com.example.moviecharactersapi.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface FranchiseRepository extends JpaRepository<Franchise,Integer> {

    @Query("select f from Franchise f where f.name like %?1%")
    Set<Franchise> findAllByName(String name);
}

