-- Movies
INSERT INTO movie (director, genre, picture, title, trailer, year) VALUES ('Peter Jackson', 'Adventure, Fantasy', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTgJCp5srWN_FT5wsm3hCliVONIOKyen6yENw10krgG&s', 'The Hobbit: An Unexpected Journey', 'https://www.imdb.com/video/vi650683417/?playlistId=tt0903624&ref_=tt_pr_ov_vi', '2012');
INSERT INTO movie (director, genre, picture, title, trailer, year) values ('Peter Jackson', 'Adventure, Fantasy','https://www.imdb.com/title/tt1170358/', 'The Hobbit: The Desolation of Smaug', 'https://www.imdb.com/video/vi2165155865?playlistId=tt1170358&ref_=tt_pr_ov_vi','2013');
INSERT INTO movie (director, genre, picture, title, trailer, year) values ('Peter Jackson', 'Adventure, Fantasy','https://upload.wikimedia.org/wikipedia/en/0/0e/The_Hobbit_-_The_Battle_of_the_Five_Armies.jpg','The Hobbit: The Battle of the five Armies', 'https://www.imdb.com/video/vi1092005657?playlistId=tt2310332&ref_=tt_pr_ov_vi', '2014');
INSERT INTO movie (director, genre, picture, title, trailer, year) values ('Peter Jackson', 'Adventure, Fantasy', 'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg', 'The lord of the rings: The fellowship of the Ring', 'https://www.imdb.com/video/vi684573465?playlistId=tt0120737&ref_=tt_pr_ov_vi', '2001');
-- For Updating movies in franchise
INSERT INTO movie (director, genre, picture, title, trailer, year) VALUES ('jon favreau', 'sci-fi','https://www.imdb.com/title/tt0371746/mediaviewer/rm1544850432/?ref_=tt_ov_i','Iron man','', 2008);
INSERT INTO movie (director, genre, picture, title, trailer, year) VALUES ('jon favreau', 'sci-fi','https://www.imdb.com/title/tt1228705/mediaviewer/rm1059163136/?ref_=tt_ov_i','Iron man 2','', 2010);
INSERT INTO movie (director, genre, picture, title, trailer, year) VALUES ('shane black', 'sci-fi','https://www.imdb.com/title/tt1300854/mediaviewer/rm520987392/?ref_=tt_ov_i','Iron man 3','', 2013);
INSERT INTO movie (director, genre, picture, title, trailer, year) VALUES ('Kenneth branagh', 'sci-fi','', 'Thor','',2011);
INSERT INTO movie (director, genre, picture, title, trailer, year) VALUES ('Alan taylor', 'sci-fi','', 'Thor: the dark World','','13');
-- Franchise
INSERT INTO franchise (description, name) VALUES ('The Hobbit is a series consisting of three high fantasy adventure films directed by Peter Jackson. The three films are The Hobbit: An Unexpected Journey , The Hobbit: The Desolation of Smaug, and The Hobbit: The Battle of the Five Armies.','The Hobbit');
INSERT INTO franchise (description, name) VALUES ('The Lord of the Rings is the saga of a group of sometimes reluctant heroes who set forth to save their world from consummate evil. Its many worlds and creatures were drawn from Tolkien''s extensive knowledge of philology and folklore', 'The Lord of the Rings');
-- For Updating movies in franchise
INSERT INTO franchise (description, name) VALUES ( 'The Marvel Cinematic Universe is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios. The films are based on characters that appear in American comic books published by Marvel Comics','Marvel Cinematic Universe');

-- Characters
INSERT INTO character (alias, gender, name, picture) VALUES ('Bilbo', 'male', 'Martin Freeman', 'https://www.imdb.com/name/nm0293509/mediaviewer/rm1317711872?ref_=nm_ov_ph');
INSERT INTO character (alias, gender, name, picture) VALUES ('Gandalf', 'male', 'Ian Mckellen', 'https://www.imdb.com/name/nm0005212/mediaviewer/rm230854144?ref_=nm_ov_ph');
insert into character (alias, gender, name, picture) VALUES ('Galadriel', 'female', 'Cate Blanchett', 'https://m.media-amazon.com/images/M/MV5BMTc1MDI0MDg1NV5BMl5BanBnXkFtZTgwMDM3OTAzMTE@._V1_UY317_CR3,0,214,317_AL_.jpg');


-- Movie_characters
INSERT INTO movie_character (movie_id, character_id) values (1, 1);
INSERT INTO movie_character (movie_id, character_id) values (1, 2);
INSERT INTO movie_character (movie_id, character_id) values (1, 3);

INSERT INTO movie_character (movie_id, character_id) values (2, 1);
INSERT INTO movie_character (movie_id, character_id) values (2, 2);
INSERT INTO movie_character (movie_id, character_id) values (2, 3);

INSERT INTO movie_character (movie_id, character_id) values (3, 1);
INSERT INTO movie_character (movie_id, character_id) values (3, 2);
INSERT INTO movie_character (movie_id, character_id) values (3, 3);

INSERT INTO movie_character (movie_id, character_id) values (4, 3);


-- Movie => franchise
UPDATE movie SET franchise_id = 1 where movie.id = 1 ;
UPDATE movie SET franchise_id = 1 where movie.id = 2 ;
UPDATE movie SET franchise_id = 1 where movie.id = 3 ;
UPDATE movie SET franchise_id = 2 where movie.id = 4 ;

